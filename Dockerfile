# DOCKER-VERSION 1.0.1

FROM   ubuntu:latest

RUN apt-get update

RUN apt-get install -y nodejs npm git git-core nodejs-legacy

ADD start.sh /tmp/

RUN chmod +x /tmp/start.sh

CMD ./tmp/start.sh
